<?php

use Illuminate\Support\Facades\Auth;
use LoiPham\Hostel\Facades\PageTitleFacade;
use LoiPham\Hostel\Supports\PageTitle;

if (!function_exists('hostel_package_path')) {
    /**
     * @param null $path
     * @return string
     * @author Loi Pham
     */
    function hostel_package_path(): string
    {
        if (File::exists(base_path('packages/hostel/'))) {
            $resultPath = base_path('packages/hostel/');
        } else {
            $resultPath = base_path('vendor/loipham/hostel/');
        }
        return $resultPath;
    }
}
