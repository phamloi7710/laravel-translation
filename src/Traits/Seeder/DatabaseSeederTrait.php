<?php

namespace LoiPham\Translation\Traits\Seeder;

use Illuminate\Console\Events\CommandFinished;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Console\Output\ConsoleOutput;
use LoiPham\Translation\Constants\TableConstant;

/**
 * Trait LoadAndPublishDataTrait
 * @package LoiPham\Traits
 * @mixin ServiceProvider
 */
trait DatabaseSeederTrait
{
    public static $seeds_path = TableConstant::SEEDER_PATH;

    /**
     * Get a value that indicates whether the current command in console
     * contains a string in the specified $fields.
     *
     * @param string|array $contain_options
     * @param string|array $exclude_options
     *
     * @return bool
     */
    public static function isConsoleCommandContains($contain_options, $exclude_options = null): bool
    {
        $args = Request::server('argv');
        if (is_array($args)) {
            $command = implode(' ', $args);
            if (str_contains($command, $contain_options) && ($exclude_options == null || !str_contains($command, $exclude_options))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add seeds from the $seed_path after the current command in console finished.
     */
    public static function addSeedsAfterConsoleCommandFinished()
    {
        Event::listen(CommandFinished::class, function (CommandFinished $event) {
            if ($event->output instanceof ConsoleOutput) {
                static::registerSeedsFrom(static::$seeds_path);
            }
        });
    }

    protected static function registerSeedsFrom($path)
    {
        foreach (glob("$path/*.php") as $filename) {
            include $filename;

            $classes = get_declared_classes();
            $class = end($classes);

            $command = Request::server('argv');
            if (is_array($command)) {
                $command = implode(' ', $command);
                if ($command == "artisan db:seed") {
                    Artisan::call('db:seed', ['--class' => $class]);
                }
            }

        }
    }
}

