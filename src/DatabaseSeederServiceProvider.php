<?php

namespace LoiPham\Translation;

use Illuminate\Support\ServiceProvider;
use LoiPham\Translation\Traits\Seeder\DatabaseSeederTrait;

class DatabaseSeederServiceProvider extends ServiceProvider
{
    use DatabaseSeederTrait;

    public function register()
    {

    }

    public function boot()
    {
        $this->loadAndActiveSeeder();
    }

    protected function loadAndActiveSeeder()
    {
        if ($this->app->runningInConsole()) {
            if (DatabaseSeederTrait::isConsoleCommandContains('db:seed')) {
                DatabaseSeederTrait::addSeedsAfterConsoleCommandFinished();
            }
        }
    }

}
