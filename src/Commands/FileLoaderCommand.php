<?php

    namespace LoiPham\Translation\Commands;

    use Illuminate\Console\Command;
    use Illuminate\Filesystem\Filesystem;
    use LoiPham\Translation\Repositories\LanguageRepository;
    use LoiPham\Translation\Repositories\TranslationRepository;

    class FileLoaderCommand extends Command
    {
        protected $path = null;
        /**
         * The console command name.
         * @var string
         */
        protected $name      = 'translator:load {--path : path to load files language}';
        protected $signature = 'translator:load {--path=}';
        /**
         * The console command description.
         * @var string
         */
        protected                                                           $description = "Load language files into the database.";
        protected \LoiPham\Translation\Repositories\LanguageRepository              $languageRepository;
        protected \LoiPham\Translation\Repositories\TranslationRepository $translationRepository;
        protected Filesystem                                              $files;
        protected                                                         $defaultLocale;

        /**
         *  Create a new mixed loader instance.
         * @param \LoiPham\Translation\Repositories\LanguageRepository $languageRepository
         * @param \LoiPham\Translation\Repositories\TranslationRepository $translationRepository
         * @param \Illuminate\Filesystem\Filesystem $files
         * @param $translationsPath
         * @param $defaultLocale
         */
        public function __construct(LanguageRepository $languageRepository, TranslationRepository $translationRepository, Filesystem $files, $translationsPath, $defaultLocale)
        {
            parent::__construct();
            $this->languageRepository    = $languageRepository;
            $this->translationRepository = $translationRepository;
            $this->path                  = (!$this->path) ? $translationsPath : $this->path;
            $this->files                 = $files;
            $this->defaultLocale         = $defaultLocale;
        }

        public function handle()
        {
            $path = $this->option('path');
            if ($path) {
                $this->path = base_path() . '/' . $path;
            }
            return $this->fire();
        }

        /**
         *  Execute the console command.
         * @return void
         * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
         */
        public function fire()
        {
            $this->loadLocaleDirectories($this->path, '*');
        }

        /**
         *  Loads all locale directories in the given path (/en, /es, /fr) as long as the locale corresponds to a language in the database.
         *  If a vendor directory is found not inside another vendor directory, the files within it will be loaded with the corresponding namespace.
         * @param string $path Full path to the root directory of the locale directories. Usually /path/to/laravel/resources/lang
         * @param string $namespace Namespace where the language files should be inserted.
         * @return void
         * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
         */
        public function loadLocaleDirectories(string $path, $namespace = '*')
        {
            $availableLocales = $this->languageRepository->availableLocales();
            $directories      = $this->files->directories($path);
            foreach ($directories as $directory) {
                $locale = basename($directory);
                if (in_array($locale, $availableLocales)) {
                    $this->loadDirectory($directory, $locale, $namespace);
                }
                if ($locale === 'vendor' && $namespace === '*') {
                    $this->loadVendor($directory);
                }
            }
        }

        /**
         *  Load all vendor overriden localization packages. Calls loadLocaleDirectories with the appropriate namespace.
         * @param string $path Path to vendor locale root, usually /path/to/laravel/resources/lang/vendor.
         * @return void
         * @see    http://laravel.com/docs/5.1/localization#overriding-vendor-language-files
         */
        public function loadVendor(string $path)
        {
            $directories = $this->files->directories($path);
            foreach ($directories as $directory) {
                $namespace = basename($directory);
                $this->loadLocaleDirectories($directory, $namespace);
            }
        }

        /**
         *  Load all files inside a locale directory and its subdirectories.
         * @param string $path Path to locale root. Ex: /path/to/laravel/resources/lang/en
         * @param string $locale Locale to apply when loading the localization files.
         * @param string $namespace Namespace to apply when loading the localization files ('*' by default, or the vendor package name if not)
         * @param string $group When loading from a subdirectory, the subdirectory's name must be prepended. For example: trans('subdir/file.entry').
         * @return void
         * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
         */
        public function loadDirectory(string $path, string $locale, $namespace = '*', $group = '')
        {
            // Load all files inside subdirectories:
            $directories = $this->files->directories($path);
            foreach ($directories as $directory) {
//                $directoryName = str_replace($path . '/', '', $directory);
                $dirGroup      = $group . basename($directory) . '/';
                $this->loadDirectory($directory, $locale, $namespace, $dirGroup);
            }

            // Load all files in root:
            $files = $this->files->files($path);
            foreach ($files as $file) {
                $this->loadFile($file, $locale, $namespace, $group);
            }
        }

        /**
         *  Loads the given file into the database
         * @param string $path Full path to the localization file. For example: /path/to/laravel/resources/lang/en/auth.php
         * @param string $locale
         * @param string $namespace
         * @param string $group Relative from the locale directory's root. For example subdirectory/subdir2/
         * @return void
         * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
         */
        public function loadFile($file, string $locale, $namespace = '*', $group = '')
        {
            $group        = $group . basename($file, '.php');
            $translations = $this->files->getRequire($file);
            $this->translationRepository->loadArray($translations, $locale, $group, $namespace);
        }
    }
