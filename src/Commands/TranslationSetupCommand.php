<?php

namespace LoiPham\Translation\Commands;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Config;

class TranslationSetupCommand extends Command
{
    /**
     * @var CreateSuperUserCommand
     */
    protected $createSuperUserCommand;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $file;
    protected $signature = 'translator:setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Setup the translation package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Start the installation loipham/laravel-translation package');
        $this->call('vendor:publish', ['--tag' => 'laravel-translation']);
        $this->call('migrate');
        $this->call('db:seed', ['--class' => '\LoiPham\Translation\Databases\Seeders\LanguageTranslationTableSeeder']);
        $this->call('translator:load');
        $this->call('translator:flush');

        if ($this->confirm('Clear all application cache?', true)) {
            $this->call('config:cache');
            $this->call('cache:clear');
            $this->call('route:cache');
            $this->call('route:clear');
        }
        $this->info('The installation is complete!');
    }
}
