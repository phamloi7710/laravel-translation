<?php

    namespace LoiPham\Translation\Constants;
    
    define('ROOT', dirname(dirname(__FILE__)));
    define('PACKAGE_PATH', realpath(dirname(ROOT)));
    final class TableConstant
    {
        const LANGUAGE_TABLE               = 'f1_languages';
        const TRANSLATOR_TRANSLATION_TABLE = 'f1_translator_translations';
        const SEEDER_PATH = PACKAGE_PATH . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'seeders';
    }

