<?php

    use Illuminate\Database\Migrations\Migration;
    use LoiPham\Translation\Constants\TableConstant;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    return new class extends Migration {
        /**
         * Run the migrations.
         * @return void
         */
        public function up()
        {
            Schema::create(TableConstant::TRANSLATOR_TRANSLATION_TABLE, function(Blueprint $table){
                $table->id();
                $table->string('locale', 10);
                $table->string('namespace', 150)->default('*');
                $table->string('group', 150);
                $table->string('item', 150);
                $table->text('text');
                $table->boolean('unstable')->default(false);
                $table->boolean('locked')->default(false);
                $table->timestamps();
                $table->unique(['locale', 'namespace', 'group', 'item']);
            }
            );
        }

        /**
         * Reverse the migrations.
         * @return void
         */
        public function down()
        {
            Schema::drop(TableConstant::TRANSLATOR_TRANSLATION_TABLE);
        }
    };
