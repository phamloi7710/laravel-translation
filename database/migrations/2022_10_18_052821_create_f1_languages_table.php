<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    use LoiPham\Translation\Constants\TableConstant;

    return new class extends Migration {
        /**
         * Run the migrations.
         * @return void
         */
        public function up()
        {
            Schema::create(TableConstant::LANGUAGE_TABLE, function(Blueprint $table){
                $table->id();
                $table->string('name')->nullable();
                $table->string('locale', 10)->nullable();
                $table->enum('status', ['active', 'inActive'])->default('active');
                $table->integer('sort')->default('0');
                $table->timestamps();
                $table->unsignedBigInteger('deleted_by')->index()->nullable();
                $table->unsignedBigInteger('created_by')->index()->nullable();
                $table->unsignedBigInteger('updated_by')->index()->nullable();
                $table->softDeletes();

            }
            );
        }

        /**
         * Reverse the migrations.
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists(TableConstant::LANGUAGE_TABLE);
        }
    };
