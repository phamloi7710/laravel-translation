<?php

namespace LoiPham\Hostel\Databases\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use LoiPham\Hostel\Models\Administrator\UserPositionModel;

class UserPositionModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserPositionModel::class;

    public function definition(): array
    {
        return [
            'code' => 'POS00001',
            'name' => 'Software developer',
            'created_by' => 1,
            'created_at' => null,
            'updated_at' => null,
            'deleted_by' => null,
            'deleted_at' => null
        ];
    }
}
