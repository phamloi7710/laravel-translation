<?php

namespace LoiPham\Hostel\Databases\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use LoiPham\Hostel\Models\Administrator\AdministratorModel;

class AdministratorModelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AdministratorModel::class;

    public function definition(): array
    {
        return [
            'code' =>  Str::random(10),
            'name' => fake()->name(),
            'username' => fake()->userName(),
            'email' => fake()->safeEmail(),
            'password' => Hash::make('111111'),
            'phone_number' => rand(1000000000,99999999999),
            'gender' => 'male',
            'birthday' => '1994-04-30'
        ];
    }
}
