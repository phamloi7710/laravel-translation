<?php

    namespace LoiPham\Translation\Databases\Seeders;

    use Illuminate\Database\Seeder;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Hash;
    use LoiPham\Translation\Constants\TableConstant;

    class LanguageTranslationTableSeeder extends Seeder {
        /**
         * Run the database seeds.
         * @return void
         */
        public function run(){
            // check if table users is empty
            if(DB::table(TableConstant::LANGUAGE_TABLE)->count() == 0) {
                DB::table(TableConstant::LANGUAGE_TABLE)->insert([
                    [
                        'name'   => 'Tiếng Việt',
                        'locale' => 'vi',
                        'flag' => 'vi',
                        'default' => 'Y',
                        'status' => 'Y'
                    ],
                    [
                        'name'   => 'English',
                        'locale' => 'en',
                        'flag' => 'en',
                        'default' => 'Y',
                        'status' => 'Y'
                    ],
                ]);
                
            }
        }
    }
